#include <stdio.h>
#include <poll.h>
#include <unistd.h>
#include <stdlib.h>

#define TIMEOUT 5
#define BUF_LEN 1024

int main(void) 
{
	struct pollfd fds[2];
	int ret;
	int ans;
	int num1, num2;
	char buf[BUF_LEN];

	fds[0].fd = STDIN_FILENO;
	fds[0].events = POLLIN;

	while(1) {
		while(num1 == 0 || num1 > 10) {
			num1 = random();
		}

		while(num2 == 0 || num2 > 10) {
			num2 = random();
		}

		printf("%d x %d = ?\n", num1, num2);

		ret = poll(fds, 1, TIMEOUT * 1000);
		if(ret == -1) {
			perror("poll");
			return 1;
		}

		if(!ret) {
			printf("%d seconds elasped.\n", TIMEOUT);
			return 0;
		}

		if(fds[0].revents & POLLIN) {
			fgets(buf, BUF_LEN, stdin);
			ans = atoi(buf);
			if(ans == (num1 * num2)) {
				num1 = 0;
				num2 = 0;
			}
			
			else {
				return 0;
			}	
		}
	}
	return 0;
}
