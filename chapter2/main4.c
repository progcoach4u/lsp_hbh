#include <stdio.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>

#define TIMEOUT 5
#define BUF_LEN 1024

int main(void)
{
	struct timeval tv;
	fd_set readfds;
	int ret;
	int num1 = 0; 
	int num2 = 0;
	int ans = 0;
	
	char buff[BUF_LEN];

	tv.tv_sec = TIMEOUT;
	tv.tv_usec = 0;

	while(1) {
		while(num1 == 0 || (num1 > 10)) {
                        num1 = random();
                }

                while(num2 == 0 || (num2 > 10)) {
                        num2 = random();
                }

		printf("%d x %d = ?\n", num1, num2);

		tv.tv_sec = TIMEOUT;
		
		FD_ZERO(&readfds);
		FD_SET(STDIN_FILENO, &readfds);

		ret = select(STDIN_FILENO + 1, &readfds, NULL, NULL, &tv);	

		if(ret == -1) {
			perror("select");
			return 1;
		} else if(!ret) {
			printf("Time out");
			return 0;
		}

		if(FD_ISSET(STDIN_FILENO, &readfds) != 0) {
			fgets(buff, BUF_LEN, stdin);
			ans = atoi(buff);
			if(ans == num1 * num2) {
			}
			else {
				printf("Wrong Answer\n");
				return 0;
			}
			FD_ZERO(&readfds);
		}
		num1 = 0;
		num2 = 0;
	}

	fprintf(stderr, "This should not happen!\n");
	return 1;
}
